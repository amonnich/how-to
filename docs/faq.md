Frequently Asked Questions
==========================

Q1: Why do I get a _403_ error when I'm connecting to https://myservice.docs.cern.ch which I have just deployed?

A1: You must have an _index.md_ file in your _docs_ directory for your documentation to be displayed.
