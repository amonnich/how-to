# Advanced topics

The advanced topics cover more specfic situations concerning your **Markdown-based static documentation site with [MkDocs](https://www.mkdocs.org/)**. The following topics are currently available:

* Add [authentication and authorisation to your documentation](authentication_authorisation.md) to make it available only to specific people / groups.
* Provide [custom configuration for the Nginx server](nginx.md) that is serving your documentation on OpenShift.
* Keep all your documentation links the same if your are [migrating from GitBook on WebEOS to MkDocs on OpenShift](gitbook_on_webeos_to_mkdocs_on_openshift.md).
* Install [custom Python packages](custom_python_packages.md) that MkDocs can use when building your documentation.
* [Temporarily review your documentation on OpenShift](review/index.md) before deploying it.
<!--* Split your documentation into multiple git repositories with [Git submodules](git_submodules.md).-->
<!--* [Test your documentation locally](local.md) before you deploy it on OpenShift.-->
<!--* [Customise your documentation with MkDocs](mkdocs.md).-->
