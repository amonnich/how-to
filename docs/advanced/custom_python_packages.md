# Install custom Python packages that MkDocs can use when building your documentation

To build your documentation, a docker container is used that contains the necessary tools and packages. By default, that docker container comes with the [MkDocs Python package](https://pypi.org/project/mkdocs/) as well as the [MkDocs Material theme Python package](https://pypi.org/project/mkdocs-material).

!!! note
    Feel free to have a look at the [`Dockerfile`](https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-container/blob/master/Dockerfile) that produces the docker container that builds your documentation.

It is possible that you want to have additional Python packages available when your documentation is being built with MkDocs. This could include plugins for custom functionality, additional themes etc. You can see some examples at the [Python Package Index (PyPI) by searching for "MkDocs"](https://pypi.org/search/?q=MkDocs).

When you have your list of additional Python packages ready all you have to do is place them in a file called [`requirements.txt`](https://pip.pypa.io/en/stable/user_guide/#requirements-files) in the root folder of your documentation sources in your git repository. They will automatically be available when MkDocs builds your documentation.

!!! tip
    Python packages must be specified **one per line** in the `requirements.txt` file using the [appropriate format](https://pip.pypa.io/en/stable/reference/pip_install/#requirements-file-format) as shown in this example:

    ```
    mkdocs-exclude
    mkdocs-bibtex==0.2.2
    mkdocs-windmill
    ```

The root folder of your documentation sources in your git repository should now look something like this:

```
mkdocs.yml
requirements.txt
docs/
    index.md
```
