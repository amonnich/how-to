# Add authentication and authorisation to your documentation

In case your documentation should only be available to specific people / groups you can add a Single sign-on proxy to your OpenShift project to handle incoming traffic and configure it to only accept requests from specific e-groups.

#### A. Go to your OpenShift project

![Screenshot](/images/openshift-project-deployed-customised_URL.png)

---

#### B. Go to your OpenShift project Routes

Hover over **Applications** on the left and click on **Routes**.

![Screenshot](/images/openshift-project-routes-do.png)

---

#### C. Go to your documentation route

Click on **docs** from the list of routes. Unless you have a more complicated set-up you should only have one route in the list. The name **docs** might be different for you and it's essentially the **application name** you provided when you first [selected MkDocs and deployed your documentation](../new/deploy.md#e-fill-in-the-information).

![Screenshot](/images/openshift-project-routes-route-do-customised_URL.png)

---

#### D. Delete your documentation route

Click on the **Actions** menu at the upper right corner and click on **Delete**.

![Screenshot](/images/openshift-project-routes-route-delete-do.png)

---

Click on **Delete** to confirm that you want to delete your documentation route.

![Screenshot](/images/openshift-project-routes-route-delete-confirm-do.png)

---

#### E. Browse the catalog

Click on the **Add to Project** menu at the upper right corner and click on **Browse Catalog**.

![Screenshot](/images/openshift-project-browse_catalog-do-alternative.png)

---

![Screenshot](/images/openshift-project-browse_catalog.png)

---

#### F. Select thhe CERN SSO Proxy

Click on **cern-sso-proxy**.

![Screenshot](/images/openshift-project-browse_catalog-cern_sso_proxy-do.png)

---

![Screenshot](/images/openshift-project-browse_catalog-cern_sso_proxy-selected.png)

---

Click on **Next**.

![Screenshot](/images/openshift-project-browse_catalog-cern_sso_proxy-selected-do.png)

---

#### G. Fill in the information

* **AUTHORIZED_GROUPS**: Enter the e-groups that you want your documentation to be available to. If you are entering more than one e-group separate each e-group by a single space character. For example "service-e-group" or "service-e-group another-service-e-group".
* **SERVICE_NAME**: Enter the application name that you chose when you [deployed your documentation and selected MkDocs from the OpenShift Catalog](../new/deploy.md#e-fill-in-the-information). For example "docs". You can verify that by looking at your OpenShift project Services (at your OpenShift project hover over "Applications" on the left and click on "Services").
* **HOSTNAME_OVERRIDE**: Enter the hostname that you chose when you [customised your documentation URL](../new/route.md#e-customise-the-hostname). This is essentially the hostname, and hence the URL, of your documentation. For example `service.docs.cern.ch`.

![Screenshot](/images/openshift-project-browse_catalog-cern_sso_proxy-filled_in.png)

---

#### H. Click on **Create**

![Screenshot](/images/openshift-project-browse_catalog-cern_sso_proxy-filled_in-do.png)

---

!!! warning
    Adding the CERN SSO Proxy to your OpenShift project automatically readded the route that you deleted just before. Deleting your route in the first place however, reset the Internet visibility setting of your site. If, for example, you had configured your site to be available outside of CERN, it will now be available only to the CERN network. If that is the case and you want your documentation to be available outside of CERN, please follow the ["Make your service documentation available outside of CERN" step](../new/internet.md).

You can further configure the CERN SSO proxy to make adjustments or have a more complicated set-up (where, for example, only specific parts of your documentation are available to specific people / groups and the rest is publicly available). More information on this process can be found at the [respective OpenShift Knowledge Base article](https://cern.service-now.com/service-portal/article.do?n=KB0005442).
