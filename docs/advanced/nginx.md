# Provide custom configuration for the Nginx server that is serving your documentation on OpenShift

When your documentation site is deployed what happens behind the scenes is that OpenShift clones your git repository, builds your Markdown sources into a static documentation site with MkDocs and uses Nginx to serve it. The [Source-to-Image (S2I) toolkit and workflow](https://github.com/openshift/source-to-image) is used to achieve that. In this specific case, the [Nginx S2I toolkit](https://github.com/sclorg/nginx-container) is used, [with some modifications for MkDocs](https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-container). The Nginx S2I toolkit essentially serves static content allowing for user-provided custom configuration. The MkDocs S2I toolkit inherits that convenience. Here are the ways you can provide custom configuration for the Nginx server that is serving your documentation on OpenShift:

1. Add a completely custom `./nginx.conf` main cnfiguration file in the root directory of your git repository. This will comnpletely replace the default Nginx configuration.
2. Add a `./nginx-cfg` directory in the root directory of your git repository and in there add Nginx configuration files ending in `.conf`. The contents of all `./nginx-cfg/*.conf` files will be included in the existing default Nginx configuration.
3. Add a `./nginx-default-cfg/` directory in the root directory of your git repository and in there add Nginx configuration files ending in `.conf`. The contents of all `./nginx-default-cfg/*.conf` files will be included in the default server block of the existing default Nginx configuration.
4. Add a `./nginx-start/` directory in the root directory of your git repository and in there add shell script files ending in `.sh`. All `./nginx-start/*.sh` files will be sourced right before Nginx is launched.

More information can be found at the [Nginx container images README.md](https://github.com/sclorg/nginx-container).
