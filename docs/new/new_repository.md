# Create a new git repository on GitLab

Once you have created your site on Web Services you want to create a new git repository on GitLab to host the Markdown sources for your documentation. Since you are using MkDocs for your documentation, your git repository should be [structured accordingly](https://www.mkdocs.org/user-guide/writing-your-docs/).

#### A. Go to the [**Create a new project**](https://gitlab.cern.ch/projects/new) page

If you want to start with an example MkDocs structure, continue with [1. Import project](new_repository.md#1-import-project). If you are confident with structuring your git repository on your own, continue with [2. Blank project](new_repository.md#2-blank-project).

## 1. Import project

#### 1-B. Select the **Import project** tab

![Screenshot](/images/gitlab-create_a_new_project-import-do.png)

---

#### 1-C. Select the **git Repo by URL** option

![Screenshot](/images/gitlab-create_a_new_project-import-repo_by_URL-do.png)

---

#### 1-D. Fill in the information

* **Git repository URL**: Enter: `https://gitlab.cern.ch/authoring/documentation/s2i-mkdocs-example.git`. This will help you start with a very simple example MkDocs structure with minimal content.
* **Username (optional)**: Leave empty.
* **Password (optional)**: Leave empty.
* **Mirror repository**: Leave unchecked <input type="checkbox">.
* **Project name**: Enter a descriptive name for this git repository. For example "*My service name* documentation". This will help you and other users remember what this git repository is about.
* **Project URL**: Select one of the available options, essentially the *Groups* and *Users* you have access to on GitLab. There might already exist a group for your service, otherwise we suggest you [create one](https://gitlab.cern.ch/groups/new) first and then select it here. This, together with the **Project slug** will form the URL of your git repository.
* **Project slug**: Enter the slug of your git repository. This, together with the **Project URL**, will form the URL of your git repository. For example, if you selected `service` as the **Project URL** and `docs` as the **Project slug**, your git repository will be available at `https://gitlab.cern.ch/service/docs`.
* **Project description (optional)**: Enter a description of your git repository. For example "This is the documentation for *my service name*". This will help you and other users remember what this git repository is about.
* **Visibility Level**: Select the visibility level of your git repository. This only affects who has access to the Markdown sources for your documentation.
    * "Private" means you have to explicitily give read access to other users.
    * "Internal" means all CERN users have read access.
    * "Public" means that everyone on the Internet has read access.

!!! note
    The name and URL of your git repository are completely independent to your documentation site name and URL.

![Screenshot](/images/gitlab-create_a_new_project-import-filled_in.png)

---

#### 1-E. Click on **Create project**

![Screenshot](/images/gitlab-create_a_new_project-import-filled_in-do.png)

---

Your new git repository is now ready on GitLab. You can now go ahead and [generate a deploy token for your git repository](deploy_token.md) which you will later use to make sure OpenShift has access to it.

## 2. Blank project

#### 2-B. Select the **Blank project** tab

![Screenshot](/images/gitlab-create_a_new_project-blank-do.png)

---

#### 2-C. Fill in the information

* **Project name**: Enter a descriptive name for this git repository. For example "*My service name* documentation". This will help you and other users remember what this git repository is about.
* **Project URL**: Select one of the available options, essentially the *Groups* and *Users* you have access to on GitLab. There might already exist a group for your service, otherwise we suggest you [create one](https://gitlab.cern.ch/groups/new) first and then select it here. This, together with the **Project slug** will form the URL of your git repository.
* **Project slug**: Enter the slug of your git repository. This, together with the **Project URL**, will form the URL of your git repository. For example, if you selected `service` as the **Project URL** and `docs` as the **Project slug**, your git repository will be available at `https://gitlab.cern.ch/service/docs`.
* **Project description (optional)**: Enter a description of your git repository. For example "This is the documentation for *my service name*". This will help you and other users remember what this git repository is about.
* **Visibility Level**: Select the visibility level of your git repository. This only affects who has access to the Markdown sources for your documentation.
    * "Private" means you have to explicitily give read access to other users.
    * "Internal" means all CERN users have read access.
    * "Public" means that everyone on the Internet has read access.

![Screenshot](/images/gitlab-create_a_new_project-blank-filled_in.png)

---

#### 2-D. Click on **Create project**

![Screenshot](/images/gitlab-create_a_new_project-blank-filled_in-do.png)

---

Your new git repository is now ready on GitLab. You can now go ahead and [generate a deploy token for your git repository](deploy_token.md) which you will later use to make sure OpenShift has access to it.
