# New documentation

You are here because you want to create a **Markdown-based static documentation site with [MkDocs](https://www.mkdocs.org/)** from scratch. You can achieve that by following a few simple steps. We suggest that you start with [the first step](new_site.md) but feel free to jump ahead in the steps listed below if you know what you're doing.

1. [Create a new site on Web Services](new_site.md).
2. [Create a new git repository on GitLab](new_repository.md).
3. [Generate a deploy token for your git repository](deploy_token.md).
4. [Deploy your documentation](deploy.md).
5. [Customise your documentation URL](route.md).
6. [Automatically redeploy your documentation every time you update your git repository](redeploy.md).
7. [Make your documentation available outside of CERN](internet.md).
8. [Go <span style="color: green;">green</span>](green.md)!

Make sure to also have a look at the [advanced topics](../advanced/index.md) for more advanced how-to guides.
