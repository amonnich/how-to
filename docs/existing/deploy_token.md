# Generate a deploy key for git repository

You want to make sure that OpenShift will have access to your git repository. You can achieve that by creating a deploy token for your git repository that you will later use in OpenShift.

#### A. Go to your git repository on GitLab

Visit the URL of your git repository (for example `https://gitlab.cern.ch/service/docs`).

#### B. Go to your git repository's **Repository Settings**

Hover over **Settings** on the left and click on **Repository**.

![Screenshot](/images/gitlab-settings_repository-do.png)

---

#### C. Expand the **Deploy Tokens** section

Click on **Expand** next to the **Deploy Tokens** section.

![Screenshot](/images/gitlab-settings_repository-deploy_tokens-do.png)

---

#### D. Fill in the information

* **Name**: Enter a descriptive name for this deploy token. For example "OpenShift", since it's coming from OpenShift, or "OpenShift (service-docs)" to make it even more specific. This will help you remember what this deploy token is about.
* **Expires at**: Leave empty.
* **Username**: Leave empty and GitLab will automatically generate a username for you.
* **Scope**
    * **read_repository**: Check it <input type="checkbox" checked="checked">.
    * **read_registry**: Leave unchecked <input type="checkbox">.

![Screenshot](/images/gitlab-settings_repository-deploy_tokens-filled_in.png)

---

#### E. Click on **Create deploy token**

![Screenshot](/images/gitlab-settings_repository-deploy_tokens-filled_in-do.png)

---

#### F. Note down the deploy token username and token

The deploy token has been created. Make sure you note down the username and token that you will soon use on OpenShift to make sure it can access your git repository.

![Screenshot](/images/gitlab-settings_repository-deploy_tokens-done.png)

---

!!! info
    More information on the deploy tokens on GitLab can be found at the [respective official GitLab documentation article](https://docs.gitlab.com/ee/user/project/deploy_tokens/).

You can now move on to [deploying your documentation](deploy.md).
