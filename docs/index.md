# Documentation How-to Guide

!!! tip
    [Release 1.2 (featuring Material for MkDocs version 6.2.*) is available](advanced/releases.md#release-12)! If you've been using release 1.0 or 1.1 [check the notes on upgrading](advanced/releases.md#how-to-upgrade-from-release-10).

This how-to guide provides information on how to create a **Markdown-based static documentation site with [MkDocs](https://www.mkdocs.org/)**. Your Markdown sources will live in a git repository on GitLab and your site will be hosted on OpenShift.

* If you want to create a new documentation site from scratch or if you already have some documentation but it's not in a git repository and not available through a web site, please follow the [**New documentation** how-to guide](new/index.md).
* If you already have a documentation site in a git repository and available through a web site, please follow the [**Existing documentation** how-to guide](existing/index.md).

Make sure to also have a look at the [advanced topics](advanced/index.md) for more advanced how-to guides.

In case you want something different altogether or you just want to get in touch please contact us at <documentation-contact@cern.ch>.
